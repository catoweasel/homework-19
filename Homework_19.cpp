﻿#include <iostream>
#include "Animal.h"

int main()
{
    setlocale(LC_ALL, "rus");

    //Динамический массив указателей

    int barn = 5;
    
    Animal** p = new Animal*[barn] { new Dog, new Cat, new Horse, new Pig, new Owl };

    for (int i = 0; i < 5; i++)
    {
        p[i]->Voice();
    }

    delete[] p;

    std::cout << "\n-=Another way=-\n";

    //Статический массив указателей

    Animal* a[] = { new Dog, new Cat, new Horse, new Pig, new Owl };
    
    for (auto i : a)
    {
        i->Voice();
    }

    return 0;
}

#include <iostream>
#pragma once

class Animal
{
public:
	Animal ()
	{}

	~Animal()
	{}

	virtual void Voice()
	{
		std::cout << "\nAnimalsVoices\n";
	}
};

class Dog : public Animal
{
public: 
	Dog()
	{}

	void Voice() override
	{
		std::cout << "\nDog says woof!\n";
	}
};

class Cat : public Animal
{
public:
	Cat()
	{}

	void Voice() override
	{
		std::cout << "\nCat says meow!\n";
	}
};

class Horse : public Animal
{
public:
	Horse()
	{}

	void Voice() override
	{
		std::cout << "\nHorse says neigh!\n";
	}
};

class Pig : public Animal
{
public:
	Pig()
	{}

	void Voice() override
	{
		std::cout << "\nPig says oink!\n";
	}
};

class Owl : public Animal
{
public:
	Owl()
	{}

	void Voice() override
	{
		std::cout << "\nOwl says whoo!\n";
	}
};